<?php declare(strict_types = 1);

namespace DL2\Slim\Middleware;

use DL2\Slim\Exception;
use Slim\Http\Request;
use Slim\Http\Response;

class UserAgentRequired
{
    /**
     * @psalm-param callable(Request,Response):Response $next
     *
     * @internal
     */
    public function __invoke(Request $req, Response $res, callable $next): Response
    {
        if (!$req->getHeaderLine('user-agent')) {
            $error = [
                'message' => 'Request forbidden by administrative rules. Please make sure your request has a valid “User-Agent” header',
                'type'    => 'forbidden',
            ];

            throw new Exception($error);
        }

        return $next($req, $res);
    }
}
