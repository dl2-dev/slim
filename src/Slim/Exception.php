<?php declare(strict_types = 1);

namespace DL2\Slim;

use Psr\Http\Message\RequestInterface;

class Exception extends \Exception
{
    /** @var array<value-of<self::TYPES>,int> */
    const STATUSES = [
        'already_exists' => 409,
        'bad_request'    => 400,
        'forbidden'      => 403,
        'invalid'        => 422,
        'missing_field'  => 422,
        'missing'        => 404,
        'not_allowed'    => 405,
        'readonly'       => 423,
        'unauthorized'   => 401,
        'unknown'        => 500,
    ];

    const TYPES = [
        'already_exists',
        'bad_request',
        'forbidden',
        'invalid',
        'missing_field',
        'missing',
        'not_allowed',
        'readonly',
        'unauthorized',
        'unknown',
    ];

    /**
     * Any additional data.
     *
     * @var mixed[]
     */
    protected $data;

    /**
     * List of fields where the problem is.
     *
     * @var ?string[]
     */
    protected $fields;

    /**
     * Denote the circumstance of why the error occurred.
     *
     * @var string
     */
    protected $message;

    /**
     * HTTP status code.
     *
     * @var int
     */
    protected $status;

    /**
     * Minimal code telling what is wrong.
     *
     * @var value-of<self::TYPES>
     */
    protected $type;

    /**
     * ctor.
     *
     * @param string|array{
     *                data?:array,
     *                fields?:string[],
     *                message?:string,
     *                status?:int,
     *                type?:value-of<self::TYPES>
     *                } $message
     */
    public function __construct($message)
    {
        if ('string' === \gettype($message)) {
            $message = ['message' => $message];
        }

        /** @var array */
        $this->data = $message['data'] ?? [];

        /** @var ?string[] */
        $this->fields = $message['fields'] ?? null;

        /** @var string */
        $this->message = $message['message'] ?? 'Internal Server Error';

        /** @var value-of<self::TYPES> */
        $this->type = $message['type'] ?? 'unknown';

        /** @var int */
        $this->status = $message['status'] ?? self::STATUSES[$this->type];
    }

    /**
     * @return array{
     *                data:array,
     *                fields:?string[],
     *                message:string,
     *                status:int,
     *                type:value-of<self::TYPES>
     *                }
     */
    public function getInfo(RequestInterface $req): array
    {
        $data = \array_merge_recursive($this->data, [
            'method' => $req->getMethod(),
            'path'   => $req->getUri()->getPath(),
        ]);

        return [
            'data'    => $data,
            'fields'  => $this->fields,
            'message' => $this->message,
            'status'  => $this->status,
            'type'    => $this->type,
        ];
    }
}
