module.exports = {
  arrowParens: "always",
  overrides: [
    {
      files: "LICENSE",
      options: { parser: "markdown" },
    },
    {
      files: "*.xml",
      options: {
        parser: "html",
        printWidth: 178,
      },
    },
    {
      files: "*.php",
      options: { tabWidth: 4 },
    },
  ],
  printWidth: 120,
  quoteProps: "consistent",
  tabWidth: 2,
  trailingComma: "all",
};
