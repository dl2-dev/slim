<?php declare(strict_types = 1);

namespace DL2\Slim;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Throwable;

/**
 * Opinionated extension for `Slim\App`.
 *
 * @method \Slim\Container getContainer()
 */
class Application extends App
{
    /**
     * @var ?Application
     * @readonly
     */
    protected static $instance;

    /**
     * @param \Slim\Container|array<string,mixed> $container
     */
    final public function __construct($container = [])
    {
        if (\is_array($container)) {
            $container = \array_replace_recursive(
                [
                    'gc'  => '\\gc_collect_cycles',
                    'jwt' => [
                        'algo'    => 'HS256',
                        'class'   => '\\DL2\\Slim\\Middleware\\Authorization',
                        'enabled' => true,
                        'secret'  => \getenv('JWT_SECRET') ?: 'DL2-5L1M--DEV',
                    ],
                    'settings' => [
                        'determineRouteBeforeAppMiddleware' => true,
                        'httpVersion'                       => 2,
                    ],
                ],
                $container
            );
        }

        parent::__construct($container);

        /** @var array{class:class-string,enabled:bool} */
        $jwt = $this->getContainer()->get('jwt');

        if ($jwt['enabled']) {
            /** @psalm-suppress MixedMethodCall */
            $this->add(new $jwt['class']());
        }

        /** @var ?callable():void */
        $gc = $this->getContainer()->get('gc');

        if ($gc) {
            \register_shutdown_function($gc);
        }
    }

    /**
     * @param \Slim\Container|array<string,mixed> $container
     */
    public static function createInstance($container = []): self
    {
        if (null === self::$instance) {
            self::$instance = new static($container);
        }

        return self::$instance;
    }

    public static function getInstance(): self
    {
        return self::createInstance();
    }

    /**
     * @throws \Slim\Exception\ContainerValueNotFoundException no entry was found for `identity`
     */
    public function getIdentity(): object
    {
        /** @var object */
        return $this->getContainer()->get('identity');
    }

    /**
     * Override default `ErrorHandler`.
     */
    public function overrideErrorHandler(?callable $handler = null): self
    {
        return $this->overridePhpErrorHandler($handler, 'errorHandler');
    }

    /**
     * Override default `NotFoundHandler`.
     */
    public function overrideNotFoundHandler(?callable $handler = null): self
    {
        if (!$handler) {
            $handler = static function (): Response {
                throw new Exception(['message' => 'Not Found', 'type' => 'missing']);
            };
        }

        return $this->setErrorHandler('notFoundHandler', $handler);
    }

    /**
     * Override default `PhpErrorHandler`.
     */
    public function overridePhpErrorHandler(?callable $handler = null, ?string $errorType = null): self
    {
        if (!$errorType) {
            $errorType = 'phpErrorHandler';
        }

        if ($handler) {
            return $this->setErrorHandler($errorType, $handler);
        }

        $handler = static function (Request $req, Response $res, Throwable $err): Response {
            if (!($err instanceof Exception)) {
                $err = new Exception([
                    'message' => $err->getMessage(),
                    'status'  => 500,
                ]);
            }

            return $res->withJson(['error' => $err->getInfo($req)]);
        };

        return $this->setErrorHandler($errorType, $handler);
    }

    /**
     * @param class-string<Controller> $ctrlClass
     */
    public function use(string $endpoint, $ctrlClass): self
    {
        $this->any($endpoint, $ctrlClass)->add([$ctrlClass, 'preDispatch']);

        return $this;
    }

    protected function setErrorHandler(string $errorType, callable $handler): self
    {
        // prettier-ignore
        $this
            ->getContainer()
            ->offsetSet($errorType, static function () use ($handler): callable {
                return $handler;
            })
        ;

        return $this;
    }
}
