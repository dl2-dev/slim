<?php declare(strict_types = 1);

namespace DL2\Slim\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;

class CORS
{
    /**
     * Should be set to the allowed host from which your API can
     * be accessed with.
     *
     * @var string
     * @readonly
     */
    protected $origin;

    public function __construct(?string $origin = null)
    {
        if (!$origin) {
            /** @var string */
            $origin = $_SERVER['HTTP_ORIGIN'] ?? '';
        }

        $this->origin = $origin;
    }

    /**
     * @psalm-param callable(Request,Response):Response $next
     *
     * @internal
     */
    public function __invoke(Request $req, Response $res, callable $next): Response
    {
        $res = $res
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader(
                'Access-Control-Allow-Headers',
                'X-Requested-With, Content-Type, Accept, Origin, Authorization'
            )
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
            ->withHeader('Access-Control-Allow-Origin', $this->origin)
            ->withHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')
            ->withAddedHeader('Cache-Control', 'post-check=0, pre-check=0')
            ->withHeader('Pragma', 'no-cache')
        ;

        return $next($req, $res);
    }
}
