<?php declare(strict_types = 1);

namespace DL2\Slim\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;

class JSONP
{
    /**
     * JSONP callback name.
     *
     * @var string
     * @readonly
     */
    protected $callback;

    /**
     * @param string $callback specifies the default JSONP callback name
     */
    public function __construct(string $callback = 'callback')
    {
        $this->callback = $callback;
    }

    /**
     * @psalm-param callable(Request,Response):Response $next
     *
     * @internal
     */
    public function __invoke(Request $req, Response $res, callable $next): Response
    {
        /** @var ?string */
        $callback = $req->getQueryParam($this->callback);

        if (!$callback) {
            return $next($req, $res);
        }

        $payload = $next($req, $res)->getBody();

        // prettier-ignore
        return $res
            ->withHeader('content-type', 'application/javascript')
            ->write("/**/{$callback}({$payload})")
        ;
    }
}
